﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionAppearance : MonoBehaviour
{

    #region Unity Fields
    [SerializeField]
    private MonsterPartSet bodySet;
    [SerializeField]
    private bool isUI;
    #endregion Unity Fields

    private List<MinionAppearancePart> appearanceParts;
    private Dictionary<MonsterPartType, GameObject> queuedPrefabs;
    // Use this for initialization
    void Awake()
    {
        appearanceParts = new List<MinionAppearancePart>(GetComponentsInChildren<MinionAppearancePart>());
        queuedPrefabs = new Dictionary<MonsterPartType, GameObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddAll(Dictionary<MonsterPartType, MonsterPartData> parts)
    {
        foreach (var item in parts)
        {
            AddPart(item.Key, item.Value);
        }
    }

    public void AddPart(MonsterPartType type, MonsterPartData part)
    {
        var appPart = appearanceParts.Find(x => x.Type == type);
        var partPrefab = isUI ? part.PrefabUI : part.Prefab;
        if (appPart)
        {
            appPart.Setup(partPrefab);
        }else
        {
            // no appearance part for this type of part, so store it, maybe we'll have it later
            queuedPrefabs.Add(type, partPrefab);
        }
        
        RefreshAppearanceParts();
    }

    private void RefreshAppearanceParts()
    {
        var parts = GetComponentsInChildren<MinionAppearancePart>();
        foreach (var item in parts)
        {
            if (!appearanceParts.Contains(item))
            {
                appearanceParts.Add(item);
                if (queuedPrefabs.ContainsKey(item.Type))
                {
                    item.Setup(queuedPrefabs[item.Type]);
                    queuedPrefabs.Remove(item.Type);
                }
            }
        }
    }
}