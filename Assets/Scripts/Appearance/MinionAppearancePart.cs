﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionAppearancePart : MonoBehaviour {

    #region Unity Fields
    [SerializeField]
    private MonsterPartType type;
    [SerializeField]
    private Transform parent;
    #endregion Unity Fields

    public MonsterPartType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    private GameObject part;

    // Use this for initialization
    void Awake () {
        if (!parent)
        {
            parent = transform;
        }
	}

    public void Setup(GameObject partPrefab)
    {
        Clear();
        if (partPrefab)
        {
            part = Instantiate(partPrefab, parent, false);
        }
    }

    public void Clear()
    {
        if (!parent)
        {
            return;
        }
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }
}
