﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartAnchor : MonoBehaviour {

    #region Unity Fields
    [SerializeField]
    private MonsterPartType type;
    #endregion Unity Fields

    public MonsterPartType Type
    {
        get
        {
            return type;
        }
    }
}
