﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;

public class AttackController : MonoBehaviour {

    public GameObject _BulletPrefab;
    public EBulletSide Side = EBulletSide.Left;
    public float bulletSpeed = 1;
    private float timerToShoot;
    public float CoolDown = 2;

    public int _BulletDmg = 10;

    public Transform _BulletSpawnPos;

    public GameObject _Target;

    void Start () {
        timerToShoot = Time.fixedTime + CoolDown;
    }
	
	void Update () {

        if (GetComponent<MovementController>()._State == ECharacterState.Attacking)
        {
            if ((timerToShoot + CoolDown) < Time.fixedTime)
            {
                if (_Target != null)
                {
                    Shoot();
                    timerToShoot = Time.fixedTime;
                }
            }
        }
    }

    void Shoot()
    {
        GameObject bulletGO = Instantiate(_BulletPrefab, _BulletSpawnPos.position, transform.rotation);
        bulletGO.GetComponent<BulletController>().teamTag = this.gameObject.layer;
        bulletGO.GetComponent<BulletController>().bulletDmg = _BulletDmg;
        //bulletGO.layer = this.gameObject.layer;
        Rigidbody bulletClone = bulletGO.GetComponent<Rigidbody>();
        //if (Side == EBulletSide.Left)
            bulletClone.velocity = Vector3.Normalize((_Target.transform.position - _BulletSpawnPos.position))  * bulletSpeed;// (-transform.right) * bulletSpeed;
        //else
        //    bulletClone.velocity = (transform.right) * bulletSpeed;

        Destroy(bulletGO, 2f);
    }
}
