﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class AutoSortingOrder : MonoBehaviour {

    private SortingGroup sr;
    public int zOffset;

    void Awake()
    {
        sr = GetComponent<SortingGroup>();
        SetPosition();
    }

    void Update()
    {
        SetPosition();
    }

    void SetPosition()
    {
        sr.sortingOrder = -((int)(transform.position.y * 100f));
        sr.sortingOrder += zOffset;
    }
}
