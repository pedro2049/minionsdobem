﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public int teamTag = 0;
    public int bulletDmg = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        if (teamTag == collision.gameObject.layer)
            return;

        var hit = collision.gameObject;
        var health = hit.GetComponent<HealthController>();
        if (health != null)
        {
            health.TakeDamage(bulletDmg);
        }

        Destroy(gameObject);
    }


}
