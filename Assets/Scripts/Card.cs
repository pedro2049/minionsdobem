﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

    public Minion minion;
    public MinionAppearance appearance;
    public Text manaText;
    public Image selectedIndicator;
    Button myButton;

    public bool IsSelected { get; private set; }

    private void Awake()
    {
        myButton = GetComponent<Button>();
        RefreshManaText();
    }

    private void Update()
    {
        myButton.interactable = (minion.ManaCost() <= CardController.Instance.manaController.CurrentMana) && (IsSelected || CardController.Instance.SelectedCount < 2);
        selectedIndicator.enabled = IsSelected;
    }

    public void SelectCard()
    {
        IsSelected = !IsSelected;
        //if(CardController.Instance.manaController.TrySpendMana(minion.ManaCost()))
        //{
        //    MinionController.Instance.SelectMinion(minion);
        //    CardController.Instance.ChargeDeck(transform);
        //    Destroy(this.gameObject);
        //}
    }

    public void SetMonsterPart(MonsterPartType type, MonsterPartData part)
    {
        minion.SetMonsterPart(type, part);
        appearance.AddPart(type, part);
        RefreshManaText();
    }
    
    private void RefreshManaText()
    {
        manaText.text = minion.ManaCost().ToString();
    }
}
