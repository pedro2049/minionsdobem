﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class CardController : MonoBehaviour {

    public static CardController Instance;
    public ManaController manaController;
    public Card cardPrefab;
    public Button combineButton;
    public List<MonsterPartSet> MonsterParts;
    public PlayableDirector spawnPlayable;

    public Transform panelTransform;
    private List<Card> cards;

    public int SelectedCount { get {
            return GetSelectedCards().Count;
        } }

    private void Start()
    {
        cards = new List<Card>();
        if (Instance == null)
            Instance = this;

        NewCard();
        NewCard();
        NewCard();
        NewCard();
        NewCard();
    }

    private void Update()
    {
        var selected = GetSelectedCards();
        int totalCost = GetTotalCost(selected);
        combineButton.interactable = selected.Count == 2 && totalCost <= manaController.CurrentMana;
    }

    public bool CanCombineSelected()
    {
        var selected = GetSelectedCards();
        var cost = GetTotalCost(selected);
        return manaController.CurrentMana >= cost;
    }

    public int GetTotalCost(List<Card> selected)
    {
        var totalCost = 0;
        if (selected.Count > 0)
        {
            totalCost += selected[0].minion.ManaCost();
            if (selected.Count > 1)
            {
                totalCost += selected[1].minion.ManaCost();
            }
        }

        return totalCost;
    }

    public void ChargeDeck()
    {
        Invoke("NewCard", 3f);
    }

    public void NewCard()
    {
        var myCard = Instantiate(cardPrefab, panelTransform);
        foreach (var set in MonsterParts)
        {
            var part = set.GetRandomPart();
            myCard.SetMonsterPart(set.Type, part);
        }
        cards.Add(myCard);
    }

    public void RemoveCard(Card card)
    {
        cards.Remove(card);
        Destroy(card.gameObject);
    }

    public List<Card> GetSelectedCards()
    {
        return cards.FindAll(x => x.IsSelected);
    }

    public void CombineSelected()
    {
        var selected = GetSelectedCards();
        var cost = GetTotalCost(selected);
        if (manaController.TrySpendMana(cost))
        {
            spawnPlayable.Play();
            StartCoroutine(SpawnAfterPlayable(selected[0].minion, selected[1].minion));
            RemoveCard(selected[0]);
            RemoveCard(selected[1]);
            ChargeDeck();
            ChargeDeck();
        }
    }

    private IEnumerator SpawnAfterPlayable(Minion first, Minion second)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => spawnPlayable.state != PlayState.Playing);
        MinionController.Instance.SelectMinion(first);
        MinionController.Instance.SelectMinion(second);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
