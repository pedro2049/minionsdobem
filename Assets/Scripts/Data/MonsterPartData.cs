﻿using System;
using UnityEngine;

[System.Serializable]
public class MonsterPartData
{
    #region Unity Fields
    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private GameObject prefabUI;
    [SerializeField]
    private int value;
    [SerializeField]
    private int cost;
    [SerializeField]
    private float chance = 1f;
    #endregion Unity Fields

    public GameObject Prefab
    {
        get
        {
            return prefab;
        }

        set
        {
            prefab = value;
        }
    }

    public int Value
    {
        get
        {
            return value;
        }

        set
        {
            this.value = value;
        }
    }

    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
        }
    }

    public float Chance
    {
        get
        {
            return chance;
        }

        set
        {
            chance = value;
        }
    }

    public GameObject PrefabUI
    {
        get
        {
            return prefabUI;
        }

        set
        {
            prefabUI = value;
        }
    }
}
