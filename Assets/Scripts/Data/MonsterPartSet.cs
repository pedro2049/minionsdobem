﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName ="Minions/Monster Part Set")]
public class MonsterPartSet : ScriptableObject {
    #region Unity Fields
    [SerializeField]
    private MonsterPartType type;
    [SerializeField]
    private List<MonsterPartData> elements;
    #endregion Unity Fields

    public List<MonsterPartData> Elements
    {
        get
        {
            return elements;
        }

        set
        {
            elements = value;
        }
    }

    public MonsterPartType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public MonsterPartData GetRandomPart()
    {
        var totalChance = elements.Sum(x => x.Chance);
        var random = Random.Range(0, totalChance);
        var chosen = 0;
        for (int i = 0; i < elements.Count; i++)
        {
            if (random > elements[i].Chance)
            {
                random -= elements[i].Chance;
            }else
            {
                chosen = i;
                break;
            }
        }
        return elements[chosen];
    }
}
