﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public enum ECharacterState
    {
        Walking = 0,
        Attacking = 1,
        Running = 2
    }
}
