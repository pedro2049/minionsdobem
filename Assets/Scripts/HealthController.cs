﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthController : MonoBehaviour {

    public static event Action<HealthController> OnDeath;

    public int maxHealth = 100;
    public int currentHealth = 100;

    public bool isKing = false;
    public bool isMonster = false;

    public RectTransform healthBar;

    void Start () {
    }
	
	void Update () {
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Debug.Log("Dead!");

            if (OnDeath != null)
            {
                OnDeath(this);
            }

            if (isKing)
                SceneManager.LoadScene(2);

            healthBar.gameObject.SetActive(false);
            //Destroy(this.gameObject);
        }

        float maxHP = maxHealth;
        float curHP = currentHealth;

        healthBar.sizeDelta = new Vector2(((curHP/ maxHP)*100f), healthBar.sizeDelta.y);
    }
}
