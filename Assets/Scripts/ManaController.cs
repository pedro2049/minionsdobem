﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaController : MonoBehaviour {

    #region Unity Fields
    [SerializeField]
    private int maxMana;
    [SerializeField]
    private int startMana;
    [SerializeField]
    private float recoverSpeed;
    #endregion Unity Fields

    public float CurrentMana { get; private set; }

    public int MaxMana
    {
        get
        {
            return maxMana;
        }
    }

    private void Start () {
        CurrentMana = startMana;
	}
	
	private void Update () {
        CurrentMana += Time.deltaTime * recoverSpeed;
        CurrentMana = Mathf.Min(CurrentMana, MaxMana);
	}

    public bool TrySpendMana(float amount)
    {
        if (CurrentMana < amount)
        {
            return false;
        }
        CurrentMana -= amount;
        return true;
    }
}
