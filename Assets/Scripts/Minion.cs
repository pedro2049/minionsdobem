﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion : MonoBehaviour {

    public int life = 10;
    public int attack = 10;
    public int speed = 10;
    public int cost;

    public SpriteRenderer body;

    public Color[] attackColors;

    private Dictionary<MonsterPartType, MonsterPartData> monsterParts;

    public Dictionary<MonsterPartType, MonsterPartData> MonsterParts
    {
        get
        {
            return monsterParts;
        }
    }

    private void Awake()
    {
        monsterParts = new Dictionary<MonsterPartType, MonsterPartData>();
    }

    public int ManaCost() {
        return cost;
    }

    public void SetMonsterPart(MonsterPartType type, MonsterPartData part)
    {
        MonsterParts[type] = part;
        ApplyPartEffect(type, part);
        
    }

    private void ApplyPartEffect(MonsterPartType type, MonsterPartData data)
    {
        switch (type)
        {
            case MonsterPartType.HP:
                life = data.Value;
                break;
            case MonsterPartType.Attack:
                attack = data.Value;
                break;
            case MonsterPartType.Speed:
                speed = data.Value;
                break;
            default:
                break;
        }
        cost += data.Cost;
    }

    public MonsterPartData GetMonsterPart(MonsterPartType type)
    {
        return monsterParts[type];
    }
}
