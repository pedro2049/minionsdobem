﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionController : MonoBehaviour {

    public static MinionController Instance;

    public Minion[] minions;

    public GameObject baseMinion;

    public Transform spawner;

    public int minionsSelected = 0;

    public Minion fatherMinion;
    public Minion motherMinion;
    public float speedFactor = 0.005f;
    public float spawnOffset = 0.2f;

    private void Start()
    {
        if (Instance == null)
            Instance = this;
    }

    public void SelectMinion(int id)
    {
        minionsSelected++;

        if (minionsSelected == 1)
        {
            fatherMinion = minions[id];
        }
        else if (minionsSelected == 2)
        {
            minionsSelected = 0;
            motherMinion = minions[id];

            RandomizeMinion();
        }
    }

    public void SelectMinion(Minion minion)
    {
        minionsSelected++;

        if (minionsSelected == 1)
        {
            fatherMinion = minion;
        }
        else if (minionsSelected == 2)
        {
            minionsSelected = 0;
            motherMinion = minion;

            RandomizeMinion();
        }
    }

    void RandomizeMinion()
    {
        Vector3 randomPosition = new Vector3(spawner.position.x, spawner.position.y + Random.Range(-spawnOffset, spawnOffset), spawner.position.z);
        GameObject newMinion = Instantiate(baseMinion, randomPosition, spawner.rotation);
        newMinion.transform.parent = spawner;
        var minionComp = newMinion.GetComponent<Minion>();
        var randomParent = RandomParent();
        var appearance = newMinion.GetComponentInChildren<MinionAppearance>();
        foreach (var part in randomParent.MonsterParts)
        {
            var type = part.Key;
            var selectedPart = RandomParent().GetMonsterPart(type);
            minionComp.SetMonsterPart(type, selectedPart);
            appearance.AddPart(type, selectedPart);
        }

        newMinion.GetComponentInChildren<AttackController>()._BulletDmg = newMinion.GetComponent<Minion>().attack;
        newMinion.GetComponentInChildren<HealthController>().maxHealth = 100 + newMinion.GetComponent<Minion>().life;
        newMinion.GetComponentInChildren<HealthController>().currentHealth = newMinion.GetComponentInChildren<HealthController>().maxHealth;
        newMinion.GetComponentInChildren<MovementController>().MovementSpeed = speedFactor * newMinion.GetComponent<Minion>().speed;
        
    }

    Minion RandomParent()
    {
        if (Random.value < 0.5f)
            return fatherMinion;
        else return motherMinion;
    }

    public int GetMana(int minionID)
    {
        return minions[minionID].ManaCost();
    }
}
