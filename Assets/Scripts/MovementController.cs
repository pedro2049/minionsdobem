﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovementController : MonoBehaviour
{
    public EBulletSide WalkSide = EBulletSide.Left;
    public ECharacterState _State = ECharacterState.Walking;
    public Animator hipAnimator;

    public float MovementSpeed = 0.01f;
    private float StartTime;
    private float JourneyLength;

    void Start()
    {
        StartTime = Time.time;
    }

    private void OnEnable()
    {
        HealthController.OnDeath += HealthController_OnDeath;
    }

    private void OnDisable()
    {
        HealthController.OnDeath -= HealthController_OnDeath;
    }

    private void HealthController_OnDeath(HealthController obj)
    {
        //if(this.gameObject.GetComponent<AttackController>()._Target == null)
        if (obj.GetComponent<MovementController>() == this)
        {
            _State = ECharacterState.Running;
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            _State = ECharacterState.Walking;
        }
    }

    void Update()
    {
        if(hipAnimator != null)
            hipAnimator.SetBool("walking", _State != ECharacterState.Attacking);

        if (_State == ECharacterState.Walking)
        {    
            {
                StartTime = 0;// Time.time;

                int walkModifier = 1;
                if (WalkSide == EBulletSide.Left)
                    walkModifier = -1;
                Vector3 endMarker = new Vector3(this.transform.position.x + walkModifier, this.transform.position.y, this.transform.position.z);
                JourneyLength = Vector3.Distance(this.transform.position, endMarker);

                float distCovered = 1f * MovementSpeed;// (Time.time - StartTime) * MovementSpeed;
                float fracJourney = distCovered / JourneyLength;
                transform.position = Vector3.Lerp(this.transform.position, endMarker, fracJourney * Time.deltaTime * 100f);
            }
        }
        else if (_State == ECharacterState.Running)
        {
            if (transform.position.x > 5f || transform.position.x < -5f)
            {
                
                if (GetComponent<HealthController>().isMonster)
                    SceneManager.LoadScene(3);
                else
                    Destroy(this.gameObject);
            }

            var runVector = Vector3.left;
            if (WalkSide == EBulletSide.Left)
                runVector = Vector3.right;

            transform.position = transform.position + (runVector * MovementSpeed * 10f * Time.deltaTime * 100f);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (_State == ECharacterState.Running)
            return;
            if (collision.gameObject.GetComponent<HealthController>())
        {
            this.gameObject.GetComponent<AttackController>()._Target = collision.gameObject;
            _State = ECharacterState.Attacking;
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (_State == ECharacterState.Running)
            return;

        if (this.gameObject.GetComponent<AttackController>()._Target == null &&
             collision.gameObject.GetComponent<HealthController>())
        {
            this.gameObject.GetComponent<AttackController>()._Target = collision.gameObject;
        }

        if (this.gameObject.GetComponent<AttackController>()._Target != null)
            _State = ECharacterState.Attacking;
    }
}
