﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombinationUI : MonoBehaviour {

    #region Unity Fields

    [SerializeField]
    private Image activatedMana;
    [SerializeField]
    private Image activatedFirst;
    [SerializeField]
    private Image activatedSecond;
    [SerializeField]
    private Image activatedAll;
    [SerializeField]
    private Text manaText;
    [SerializeField]
    private MinionAppearance firstMonster;
    [SerializeField]
    private MinionAppearance secondMonster;

    [SerializeField]
    private CardController cardController;
    #endregion Unity Fields

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var selected = cardController.GetSelectedCards();
        activatedFirst.enabled = selected.Count >= 1;
        firstMonster.gameObject.SetActive(selected.Count >= 1);
        activatedSecond.enabled = selected.Count == 2;
        secondMonster.gameObject.SetActive(selected.Count == 2);

        activatedMana.enabled = cardController.CanCombineSelected();

        activatedAll.enabled = selected.Count == 2 && cardController.CanCombineSelected();
        manaText.text = cardController.GetTotalCost(selected).ToString();
        if (selected.Count >= 1)
        {
            firstMonster.AddAll(selected[0].minion.MonsterParts);
        }
        
        if (selected.Count == 2)
        {
            secondMonster.AddAll(selected[1].minion.MonsterParts);
        }
        
    }
}
