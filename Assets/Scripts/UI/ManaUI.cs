﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaUI : MonoBehaviour {

    #region Unity Fields
    [SerializeField]
    private Image progressBar;
    [SerializeField]
    private Text manaLabel;
    [SerializeField]
    private ManaController mana;
    [SerializeField]
    private int fillHighlightStops = 10;
    [SerializeField]
    private GameObject fillHighlightStopPrefab;
    [SerializeField]
    private Animator imageAnimator;
    #endregion Unity Fields

    private int lastFillStop;

    // Use this for initialization
    void Start () {
        var width = progressBar.rectTransform.rect.width;
        var stopSpacing = width / (float) fillHighlightStops;
        for (int i = 1; i < fillHighlightStops; i++)
        {
            var instance = Instantiate(fillHighlightStopPrefab, progressBar.transform);
            instance.GetComponent<RectTransform>().anchoredPosition = new Vector2(i * stopSpacing, 0);
        }
	}
	
	// Update is called once per frame
	void Update () {
        var newFill = mana.CurrentMana / mana.MaxMana;
        progressBar.fillAmount = newFill;
        var currentFillStop = 0;
        for (int i = 0; i < fillHighlightStops; i++)
        {
            var stopRef = i / (float) fillHighlightStops;
            if (newFill > stopRef)
            {
                currentFillStop = i;
            }
        }
        if (currentFillStop != lastFillStop)
        {
            imageAnimator.SetTrigger("Bounce");
            lastFillStop = currentFillStop;
        }
        manaLabel.text = Mathf.FloorToInt(mana.CurrentMana).ToString();
    }
}
