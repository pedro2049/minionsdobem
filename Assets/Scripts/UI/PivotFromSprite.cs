﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[ExecuteInEditMode]
public class PivotFromSprite : MonoBehaviour {

    private Image image;
	
	void Awake () {
        image = GetComponent<Image>();
    }
	
	
	void Update () {
        if (image && image.sprite)
        {
            Vector2 size = image.rectTransform.sizeDelta;
            Vector2 pixelPivot = image.sprite.pivot;
            Vector2 percentPivot = new Vector2(pixelPivot.x / size.x, pixelPivot.y / size.y);
            image.rectTransform.pivot = percentPivot;
        }
    }
}
